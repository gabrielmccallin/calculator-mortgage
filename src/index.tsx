import { Global, Interpolation } from "@emotion/core";
import React from "react";
import { render } from "react-dom";
import { Parameter } from "./Parameter";
import { styles } from "./Styles";
import { Result } from "./Result";
import { Title } from "./Title";

const GlobalStyles = () => (
  <Global
    styles={{
      'body': {
        margin: 0,
        fontSize: '1rem',
        fontFamily: 'Arial, Helvetica, sans-serif',
        backgroundColor: styles.backgroundColour,
        color: styles.primaryColour
      },
      '*': {
        boxSizing: 'border-box'
      }
    }}
  />
);

const App: React.FC = () => {
  const desc = 'Enter details to see monthly repayments and money left over. Results are approximate.';
  const monthlyFactor = 293;
  const monthlyLtvBaseline = 85;
  const monthlySpread = 60;

  const savings = 100000;
  const mortgage = 440000;
  const house = 500000;
  const ltv = 85;
  const moveCost = 5000;

  const lineStyle: Interpolation = {
    height: '1px',
    backgroundColor: styles.lineColour,
    marginBottom: '2rem'
  }

  const calculate = ({ mortgage, ltv, house, savings, left, monthly }) => {

    const stampDuty = house * 0.03;
    const deposit = house * ((100 - ltv) / 100);

    left = savings - stampDuty - moveCost - deposit;

    const actualMortgage = house - deposit;
    if (actualMortgage > mortgage) {
      left = left - (actualMortgage - mortgage);
    }

    monthly = (actualMortgage / monthlyFactor) + ((ltv - monthlyLtvBaseline) * monthlySpread);

    return {
      mortgage,
      ltv,
      monthly,
      house,
      left,
      savings
    }
  };

  const reducer = (state, action) => {
    switch (action.type) {
      case 'mortgage':
        return calculate({ ...state, mortgage: action.value });
      case 'house':
        return calculate({ ...state, house: action.value });
      case 'ltv':
        return calculate({ ...state, ltv: action.value });
      case 'savings':
        return calculate({ ...state, savings: action.value });

      default:
        return state;
    }
  };

  const [state, dispatch] = React.useReducer(reducer, calculate({ mortgage, ltv, house, savings, left: 0, monthly: 0 }));

  return (
    <React.Fragment>
      <GlobalStyles />

      <div css={{
        display: 'flex',
        height: '100vh',
        margin: '2rem'
      }}
      >
        <div css={{ width: '100%', maxWidth: '500px' }}>
          <Title desc={desc} title="Mortgage calculator"></Title>

          <Parameter title='Cost of house (£)' id="calc-house" name="house" onChange={value => dispatch({ type: "house", value })} value={(state.house).toString()} />
          <Parameter title='Maximum borrowing (£)' id="calc-mortgage" name="mortgage" onChange={value => dispatch({ type: "mortgage", value })} value={state.mortgage.toString()} />
          <Parameter title="Savings (£)" id="calc-savings" name="savings" onChange={value => dispatch({ type: "savings", value })} value={state.savings.toString()} />
          <Parameter title='Loan to value (%)' id="calc-ltv" name="ltv" onChange={value => dispatch({ type: "ltv", value })} value={state.ltv.toString()} />
          <div css={lineStyle}></div>
          <div css={{ marginBottom: "10rem" }}>
            <Result title="Monthly payments (£ / month)" id="result-monthly" value={state.monthly.toFixed(2)} desc="Repayment" />
            <Result title='Money left over (£)' id="result-left" desc="Includes stamp duty, legal fees, survey and removal costs" value={state.left.toFixed(0)} />
          </div>
        </div>
      </div>
    </React.Fragment>
  )
};

render(<App />, document.getElementById("root"));
