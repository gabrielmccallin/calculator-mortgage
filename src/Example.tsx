import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { css, jsx } from '@emotion/core';

const App = () => {

  const globalStyle = css({
    fontSize: '3rem',
    fontFamily: 'sans-serif',
    backgroundColor: 'grey',
    color: 'white'
  });

  const [state, setState] = React.useState({ mortgage: '100000', monthly: (Number(100000) / 280).toFixed(3) });
  const onChange = (e) => {

    switch (e.target.name) {
      case 'mortgage':
        const monthly = (Number(e.target.value) / 280).toFixed(3);
        setState({ mortgage: e.target.value, monthly: monthly });
        break;

      case 'monthly':
        setState({ mortgage: state.mortgage, monthly: e.target.value });
        break;

      default:
        break;
    }
  };

  return (
    <div className="App" css={globalStyle}>
      <label css={{
        color: 'darkorchid',
        backgroundColor: 'lightgray'
      }}>
        Mortgage
        <input type="text" id="mortgage" name="mortgage" value={state.mortgage} onChange={onChange} />
      </label>
      <label>
        Monthly
        <input type="text" id="monthly" name="monthly" value={state.monthly} onChange={onChange} />
      </label>
    </div>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));