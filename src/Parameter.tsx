import { Interpolation } from "@emotion/serialize";
import React from "react";
import { styles } from "./Styles";

export const Parameter = ({ id, name, onChange, value, title }: { id: string, name: string, value: string, onChange: (e) => void, title: string }) => {

  const inputStyle: Interpolation = {
    display: 'block',
    fontSize: '1.2rem',
    border: `1px solid #e9d3cb`,
    borderRadius: "0.3rem",
    padding: '0.5rem',
    outline: 'none',
    marginTop: '0.5rem',
    width: '100%',
    backgroundColor: 'transparent',
    color: styles.secondaryColour
  };

  const labelStyle: Interpolation = {
    marginBottom: '2.4rem',
    display: 'block'
  };

  const onChangeGuard = e => {
    if (e.target.value.match(/^[0-9]+$/)) {
      onChange(e.target.value);
    }
  };

  const onInput = (e: React.KeyboardEvent<HTMLInputElement>) => {
    if (e.key === 'Enter') {
      const target = e.currentTarget as HTMLInputElement;
      console.log(target.blur);
      target.blur();
    }
  }

  return (
    <label css={labelStyle}>
      {title}
      <input css={inputStyle} type="number" id={id} name={name} value={value} onChange={onChangeGuard} onKeyDown={onInput} />
    </label>
  )
};

