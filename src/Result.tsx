import { Interpolation } from "@emotion/serialize";
import React from "react";
import { styles } from "./Styles";

export const Result = ({ id, value, title, desc }: { id: string, value: string, title: string, desc: string }) => {

  const resultStyle: Interpolation = {
    fontSize: '2rem',
    marginTop: '0.5rem',
    color: styles.secondaryColour
  };
  
  const descStyle: Interpolation = {
    fontSize: '0.8rem',
    // marginTop: '0.5rem',
    // width: '100%',
    color: styles.tertiaryColour
  };
  
  const titleStyle: Interpolation = {
    marginBottom: '0.3rem'
  }

  const groupStyle: Interpolation = {
    marginBottom: '1.2rem'
  }

  return (
    <div css={groupStyle}>
      <div css={titleStyle}>
        {title}
      </div>
      <div css={descStyle}>
        {desc}
      </div>
      <div css={resultStyle} id={id}>{value}</div>
    </div>
  )
};

