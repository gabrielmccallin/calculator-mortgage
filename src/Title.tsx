import { Interpolation } from "@emotion/serialize";
import React from "react";
import { styles } from "./Styles";

export const Title = ({ desc, title }: { desc: string, title: string }) => {

  const titleStyle: Interpolation = {
    fontSize: '2rem',
    color: styles.secondaryColour,
    marginBottom: '0.5rem'
  };

  const descStyle: Interpolation = {
    color: styles.tertiaryColour
  }

  const groupStyle: Interpolation = {
    marginBottom: '2.4rem'
  }

  return (
    <div css={groupStyle}>
      <div css={titleStyle}>{title}</div>
      <div css={descStyle}>{desc}</div>
    </div>
  )
};

